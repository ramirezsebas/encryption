import prompt from "prompt";

function encript(text, change = 1) {
  const alphabetLowerCase = "abcdefghijklmnopqrstuvwxyz";
  const alphabetUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  let newText = "";

  for (let index = 0; index < text.length; index++) {
    const letter = text[index];

    //If letter is lowerCase
    if (alphabetLowerCase.includes(letter)) {
      let indexLetter = alphabetLowerCase.indexOf(letter);
      newText = verifJumps(indexLetter, alphabetLowerCase, newText);
    } else if (alphabetUpperCase.includes(letter)) {
      let indexLetter = alphabetUpperCase.indexOf(letter);
      newText = verifJumps(indexLetter, alphabetUpperCase, newText);
    } else {
      //If its not a letter
      newText += letter;
    }
  }
  return newText;

  function verifJumps(indexLetter, alphabet, newText) {
    if (indexLetter + change > alphabet.length - 1) {
      newText += alphabet.charAt(
        Math.abs(alphabet.length - (indexLetter + change))
      );
    } else {
      newText += alphabet.charAt(indexLetter + change);
    }
    return newText;
  }
}

//Add User Input
var schema = {
  properties: {
    password: {
      hidden: true,
      required: true,
    },
    jump: {
      pattern: /^\d+$/,
      message: "Jump must be a number",
      required: true,
    },
  },
};

prompt.start();

prompt.get(schema, function (err, result) {
  let finalResult = encript(result.password, Number(result.jump));

  console.log("  Password " + finalResult);
  console.log("  Jump: " + result.jump);
});
